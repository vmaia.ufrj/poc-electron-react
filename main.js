const { app, BrowserWindow } = require('electron');
const path = require('path');
const isDev = require('electron-is-dev');
const { spawn } = require('child_process');

const closeServer = require('./storage/server.js');

const npmPath = 'C:\\Program Files\\nodejs\\npm.cmd';
let sqliteServerRunning = false;

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  if (isDev) {
    win.loadURL('http://localhost:3000');
    win.webContents.openDevTools();
  } else {
    win.loadFile(path.join(__dirname, 'build/index.html'));
  }

  win.on('closed', () => {
    app.quit();
  });

}

app.whenReady().then(createWindow);

app.on('before-quit', () => {
  console.log('Kill');
  closeServer();
});
