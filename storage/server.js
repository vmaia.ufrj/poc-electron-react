const express = require('express');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();
const mysql = require('mysql2');
const app = express();
const port = 3001;

app.use(express.json());
app.use(cors());

const mysqlConfig = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'poc_electron',
};

const db = new sqlite3.Database('./file.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Conectado ao banco de dados SQLite.');

    db.run('CREATE TABLE IF NOT EXISTS contador (id INTEGER PRIMARY KEY, value INTEGER DEFAULT 0)', (err) => {
        if (err) {
            return console.log(err.message);
        }
        console.log('Tabela "contador" criada!');
        synchronizeContador();
    });

    db.run('CREATE TABLE IF NOT EXISTS pessoas (name TEXT PRIMARY KEY, age INTEGER)', (err) => {
        if (err) {
            return console.log(err.message);
        }
        console.log('Tabela "pessoas" criada!');
        synchronizePessoas();
    });
});

function synchronizeContador() {
    const mysqlConnection = mysql.createConnection(mysqlConfig);
    mysqlConnection.connect((err) => {
        if (err) {
            console.error('Erro ao conectar ao MySQL:', err.message);
            return;
        }

        console.log('Contador: Conectado ao MySQL para sincronização inicial.');
        db.get('SELECT value FROM contador WHERE id = 1', (sqliteErr, row) => {
            if (sqliteErr) {
                mysqlConnection.end();
                console.error('Erro ao ler do SQLite:', sqliteErr.message);
                return;
            }
            const sqliteValue = row ? row.value : 0;
            mysqlConnection.query('REPLACE INTO contador (id, value) VALUES (1, ?)', [sqliteValue], (mysqlErr) => {
                mysqlConnection.end();
                if (mysqlErr) {
                    console.error('Erro ao sincronizar com o MySQL:', mysqlErr.message);
                } else {
                    console.log('Contador: Sincronização inicial com o MySQL concluída.');
                }
            });
        });
    });
}

function synchronizePessoas() {
    const mysqlConnection = mysql.createConnection(mysqlConfig);
    mysqlConnection.connect(err => {
        if (err) {
            console.error('Erro ao conectar ao MySQL para sincronizar pessoas:', err.message);
            return;
        }

        console.log('Pessoas: Conectado ao MySQL para sincronização inicial.');
        mysqlConnection.query('SELECT * FROM pessoas', (mysqlErr, results) => {
            if (mysqlErr) {
                mysqlConnection.end();
                console.error('Erro ao ler da tabela pessoas no MySQL:', mysqlErr.message);
                return;
            }

            db.run('DELETE FROM pessoas', (sqliteErr) => {
                if (sqliteErr) {
                    console.error('Erro ao limpar tabela pessoas no SQLite:', sqliteErr.message);
                    return;
                }

                results.forEach(person => {
                    db.run('INSERT INTO pessoas (name, age) VALUES (?, ?)', [person.name, person.age], sqliteErr => {
                        if (sqliteErr) {
                            console.error('Erro ao inserir dados no SQLite:', sqliteErr.message);
                        }
                    });
                });
            });

            mysqlConnection.end(() => {
                console.log('Pessoas: Sincronização inicial com o MySQL concluída.');
            });
        });
    });
}

function updateCounter(value, res) {
    db.run('REPLACE INTO contador (id, value) VALUES (1, ?)', [value], (sqliteErr) => {
        if (sqliteErr) {
            res.status(500).send(sqliteErr.message);
            return;
        }
        const mysqlConnection = mysql.createConnection(mysqlConfig);
        mysqlConnection.connect((err) => {
            if (err) {
                console.error('Erro ao conectar ao MySQL:', err.message);
                res.send({ id: 1, value: value });
            } else {
                mysqlConnection.query('REPLACE INTO contador (id, value) VALUES (1, ?)', [value], (mysqlErr) => {
                    mysqlConnection.end();
                    if (mysqlErr) {
                        console.error('Erro ao atualizar o contador no MySQL:', mysqlErr.message);
                        res.status(500).send(mysqlErr.message);
                        return;
                    }
                    res.send({ id: 1, value: value });
                });
            }
        });
    });
}

// ENDPOINTS para o CONTADOR.

app.post('/increment', (req, res) => {
    db.get('SELECT value FROM contador WHERE id = 1', (sqliteErr, row) => {
        if (sqliteErr) {
            res.status(500).send(sqliteErr.message);
            return;
        }
        const newValue = row ? row.value + 1 : 1;
        updateCounter(newValue, res);
    });
});

app.post('/reset', (req, res) => {
    updateCounter(0, res);
});

app.get('/count', (req, res) => {
    db.get('SELECT value FROM contador WHERE id = 1', (err, row) => {
        if (err) {
            res.status(500).send(err.message);
            return;
        }
        res.send({ total: row ? row.value : 0 });
    });
});

// ENDPOINTS para as PESSOAS.

app.post('/setPersonAge', (req, res) => {
    const { name, age } = req.body;
    db.run('REPLACE INTO pessoas (name, age) VALUES (?, ?)', [name, age], (sqliteErr) => {
        if (sqliteErr) {
            res.status(500).send(sqliteErr.message);
            return;
        }
        const mysqlConnection = mysql.createConnection(mysqlConfig);
        mysqlConnection.connect((err) => {
            if (err) {
                console.error('Erro ao conectar ao MySQL:', err.message);
                res.send({ status: 'Atualizado apenas no SQLite.' });
            } else {
                mysqlConnection.query('REPLACE INTO pessoas (name, age) VALUES (?, ?)', [name, age], (mysqlErr) => {
                    mysqlConnection.end();
                    if (mysqlErr) {
                        console.error('Erro ao atualizar no MySQL:', mysqlErr.message);
                        res.status(500).send(mysqlErr.message);
                        return;
                    }
                    res.send({ status: 'Atualizado nos dois bancos.' });
                });
            }
        });
    });
});

app.get('/getPersonAge', (req, res) => {
    const { name } = req.query;
    db.get('SELECT age FROM pessoas WHERE name = ?', [name], (err, row) => {
        if (err) {
            res.status(500).send(err.message);
            return;
        }
        if (row) {
            res.send({ age: row.age });
        } else {
            res.status(404).send('Pessoa não encontrada');
        }
    });
});

app.get('/pessoas', (req, res) => {
    db.all('SELECT * FROM pessoas ORDER BY name', (err, rows) => {
        if (err) {
            res.status(500).send(err.message);
            return;
        }
        res.json(rows);
    });
});

app.listen(port, () => {
    console.log(`Servidor rodando em http://localhost:${port}`);
});

function closeServer() {
    server.close(() => {
        console.log('Servidor Express encerrado');
    });
}

module.exports = closeServer;
