export const style = {
  container: {
    textAlign: 'center',
    marginTop: '50px',
  },
  button: {
    marginTop: '10px',
    padding: '10px 20px',
    fontSize: '16px',
    marginRight: '5px',
  },
  table: {
    marginTop: '20px',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderCollapse: 'collapse',
    width: '50%',
  },
  th: {
    border: '1px solid black',
    padding: '8px',
    backgroundColor: '#ddd',
  },
  td: {
    border: '1px solid black',
    padding: '8px',
  },
  ageTd: {
    width: '100px',
    border: '1px solid black',
    padding: '8px',
  },
  input: {
    width: '100px',
    boxSizing: 'border-box',
  }
};
