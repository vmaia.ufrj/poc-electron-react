export const fetchCount = (setCount) => {
  fetch('http://localhost:3001/count')
    .then(response => response.json())
    .then(data => {
      if (data.total != null) {
        setCount(data.total);
      }
    })
    .catch(err => console.error('Erro ao obter o contador!', err));
};

export const incrementCount = (count, setCount) => {
  fetch('http://localhost:3001/increment', { method: 'POST' })
    .then(response => response.json())
    .then(data => {
      if (data.id) {
        setCount(count + 1);
      }
    })
    .catch(err => console.error('Erro ao incrementar o contador!', err));
};

export const resetCount = (setCount) => {
  fetch('http://localhost:3001/reset', { method: 'POST' })
    .then(response => response.json())
    .then(() => setCount(0))
    .catch(err => console.error('Erro ao resetar o contador!', err));
};

export const setPersonAge = (name, age) => {
  return fetch('http://localhost:3001/setAge', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ name, age }),
  });
};

export const getPersonAge = (name) => {
  return fetch(`http://localhost:3001/getAge?name=${encodeURIComponent(name)}`)
    .then(response => {
      if (!response.ok) {
        throw new Error('Falha ao buscar a idade!');
      }
      return response.json();
    });
};

export const fetchPessoas = () => {
  return fetch('http://localhost:3001/pessoas')
    .then(response => {
      if (!response.ok) {
        throw new Error('Falha ao buscar dados de pessoas!');
      }
      return response.json();
    });
};
