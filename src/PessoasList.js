import React, { useState, useEffect } from 'react';
import { style } from './styles';
import { fetchPessoas } from './FetchUtils';

function PessoasList() {
  const [pessoas, setPessoas] = useState([]);
  const [editingIndex, setEditingIndex] = useState(null);

  useEffect(() => {
    fetchPessoas()
      .then(data => setPessoas(data))
      .catch(err => console.error('Erro ao carregar pessoas', err));
  }, []);

  const handleAgeChange = (e, index) => {
    const newAge = e.target.value;
    const person = pessoas[index];

    fetch(`http://localhost:3001/setPersonAge`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name: person.name, age: newAge })
    })
    .then(() => {
      setPessoas(prevPessoas => prevPessoas.map((p, i) =>
        i === index ? { ...p, age: newAge } : p
      ));
      setEditingIndex(null);
    })
    .catch(err => console.error('Erro ao atualizar a idade', err));
  };

  const finishEditing = () => {
    setEditingIndex(null);
  };

  return (
    <table style={style.table}>
      <thead>
        <tr>
          <th style={style.th}>Nome</th>
          <th style={style.th}>Idade</th>
        </tr>
      </thead>
      <tbody>
        {pessoas.map((person, index) => (
          <tr key={person.name}> {}
            <td style={style.td}>{person.name}</td>
            <td style={style.ageTd}>
              {editingIndex === index ? (
                <input
                  style={style.input}
                  type="number"
                  value={person.age}
                  onChange={(e) => handleAgeChange(e, index)}
                  onBlur={finishEditing}
                />
              ) : (
                <span onClick={() => setEditingIndex(index)}>{person.age}</span>
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default PessoasList;
