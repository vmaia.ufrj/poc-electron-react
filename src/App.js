import React, { useState, useEffect } from 'react';
import Counter from './Counter';
import PessoasList from './PessoasList';
import { fetchCount, incrementCount, resetCount } from './FetchUtils';
import { style } from './styles';

function App() {
  const [count, setCount] = useState(0);
  const [pessoas, setPessoas] = useState([
    { name: '', age: 0 }
  ]);

  useEffect(() => { fetchCount(setCount); }, []);

  return (
    <div style={style.container}>
      <Counter count={count} increment={() => incrementCount(count, setCount)} reset={() => resetCount(setCount)} />
      <PessoasList pessoas={pessoas} setPessoas={setPessoas} />
    </div>
  );
}

export default App;
