import React from 'react';
import { style } from './styles';

function Counter({ count, increment, reset }) {
  return (
    <>
      <p>{count}</p>
      <button style={style.button} onClick={increment}>Incrementar</button>
      <button style={style.button} onClick={reset}>Zerar</button>
    </>
  );
}

export default Counter;
