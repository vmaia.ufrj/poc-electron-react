# Este código é MySQL!
# As tabelas possuem o mesmo formato tanto aqui quanto no SQLite.

drop schema if exists poc_electron;
create schema if not exists poc_electron;
use poc_electron;

CREATE TABLE contador (
	id INTEGER PRIMARY KEY, # A aplicação sempre usa apenas o id 1.
    value INTEGER
);

CREATE TABLE IF NOT EXISTS pessoas (
	name VARCHAR(20) PRIMARY KEY, # Apenas para simplificar.
    age INTEGER
);
INSERT INTO pessoas (name,age) VALUES ("Alice", 20);
INSERT INTO pessoas (name,age) VALUES ("Bob", 25);
INSERT INTO pessoas (name,age) VALUES ("Carlos", 15);

select * from pessoas;
