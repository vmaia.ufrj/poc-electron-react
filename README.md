# POC: Electron com React

O objetivo deste projeto **Proof of Concept** é verificar a viabilidade de utilizar [Electron](https://www.electronjs.org/pt/) para cumprir os seguintes objetivos:
- Desenvolver uma única aplicação capaz de ser executada como **Desktop** ou **Web**.
- Garantir que esta aplicação funcione tanto **online** quanto **offline** em qualquer forma que esteja sendo executada.

## Descrição da Aplicação

A aplicação desenvolvida possui apenas duas funcionalidades:
- Um contador, que pode ser incrementado ou zerado,
- Uma tabela de nomes de pessoas e suas idades.

A aplicação supõe a existência de um banco de dados **MySQL**. Caso o banco MySQL **esteja acessível**, os valores do contador e das idades são armazenados tanto no MySQL quanto em um banco local SQLite. Caso o banco MySQL **não esteja acessível**, apenas o banco local SQLite é usado, sem impactar nas funcionalidades do ponto de vista do usuário. Ao **voltar a ser acessível**, o banco MySQL é sincronizado com os valores armazenados no banco local SQLite.

## Pré-requisitos

- [Node.js](https://nodejs.org/)
- [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/), OBS: no Windows, pode-se facilitar a instalação com o [XAMPP](https://www.apachefriends.org/pt_br/index.html).
- SQLite (geralmente já incluído na instalação do Node)

## Instalação

```bash
git clone https://gitlab.com/vmaia.ufrj/poc-electron-react.git
cd poc-electron-react
npm install
```

Modifique **user** e **password** do seu banco de dados **MySQL** no arquivo ```storage/server.js```. O Script para criar o banco de dados desta aplicação está presente no arquivo ```BD Scripts/create_database.sql```.

### Executar a Aplicação

Em abas diferentes, mantenha todos estes comandos em execução.

Para executar a aplicação "back-end" que mantém o SQLite:
```bash
npm run start-sqlite
```
Para executar a aplicação Web. Exige o comando anterior:
```bash
npm run start
```
Para executar a aplicação Desktop, com Electron. Exige os dois comandos anteriores:
```bash
npm run build
npm run start-electron
```

Para gerar um executável para Windows:
```bash
npm run build
npm run package
```

Em caso de erros, apague as pastas build e node-modules, e tente novamente.

A aplicação web executa em ```localhost:3000```. A aplicação SQLite executa em ```localhost:3001```. A aplicação **Electron** abrirá uma nova janela, com o mesmo conteúdo da aplicação web. Quanto ao **executável**, basta executá-lo sem pré-requisitos.

### Cuidados para o Futuro

A maior parte da aplicação será desenvolvida com **React**, e isso permite que ela seja executada tanto como Web quanto como Desktop através do **Electron**.

Porém, o **Electron** possui funcionalidades próprias, que integram a aplicação aos processos do sistema operacional, criam janelas nativas, e manipulam o sistema de arquivos. Para que a aplicação funcione da mesma maneira como Web ou Desktop, é importante que se dê preferências às funcionalidades do **React** em vez das do **Electron** sempre que possível.
